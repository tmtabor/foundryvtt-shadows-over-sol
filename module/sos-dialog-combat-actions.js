import { SoSUtility } from "./sos-utility.js";

/* -------------------------------------------- */
export class SoSDialogCombatActions extends Dialog {
  
  /* -------------------------------------------- */
  static async create( combatId, combatantId, round, uniqId  ) {
    let combat = game.combats.get( combatId);

    let combatActions = {
      actionsList: await SoSUtility.loadCompendium( 'foundryvtt-shadows-over-sol.combat-actions' ),
      actionPoints: SoSUtility.fillRange(0, 6),
      combatId: combatId,      
      combatantId: combatantId,
      combatantsList: combat.data.combatants,
      uniqId: uniqId,
      round: round
    }
    for ( let i=0; i<combatActions.actionsList.length; i++) {
      if ( combatActions.actionsList[i].name == 'No Action') {
        combatActions.noActionId = i;
      }
    }

    //console.log("ACTIONS", combatActions.actionsList );
    let html = await renderTemplate('systems/foundryvtt-shadows-over-sol/templates/dialog-combat-actions.html', combatActions);
    return  new SoSDialogCombatActions(combatActions, html , { width: 640, height: 320} );
  }    

  /* -------------------------------------------- */
  constructor(combatActions, html, options = {} ) {
    let dialogConf = {
      title: "Combat Actions",
      content: html,
      buttons: {
        validate: {
          icon: '<i class="fas fa-check"></i>',
          label: "Validate",
          callback: (html) => { this.validateActions(html) }
        }
      },
      default: "validate"
    };
    super(dialogConf, options);

    this.combatActions = combatActions;
  }

  /* -------------------------------------------- */
  async close() {
    let ap = Number($('#remain-ap').text());
    if ( ap >= 0 ) { 
      super.close();

      let action3Index = $('#action3').val();
      let action3 = duplicate(this.combatActions.actionsList[action3Index]);
      let action2Index = $('#action2').val();
      let action2 = duplicate(this.combatActions.actionsList[action2Index]);
      let action1Index = $('#action1').val();
      let action1 = duplicate(this.combatActions.actionsList[action1Index]);

      let combatant3Id = $('#combatant3').val();
      let combatant2Id = $('#combatant2').val();
      let combatant1Id = $('#combatant1').val();
      
      let msgdata = {
        combatId: this.combatActions.combatId,
        combatantId: this.combatActions.combatantId,
        uniqId: this.combatActions.uniqId,
        userId: game.userId,
        phaseArray: [ action1, action2, action3],
        targetArray: [ combatant1Id, combatant2Id, combatant3Id],
        remainingAP: ap
       }

      if ( game.user.isGM ) {
        let combat = game.combats.get( this.combatActions.combatId ); // Get the associated  combat
        combat.setupActorActions( msgdata );
      } else {
        game.socket.emit("system.foundryvtt-shadows-over-sol", {
            name: "msg_declare_actions", data: msgdata  } );
      }
    } else {
      ui.notifications.warn("Action Points are below 0 ! Please check your phases !");
    }
  }

  /* -------------------------------------------- */
  validateActions( html ) {

  }

  /* -------------------------------------------- */
  resetAP() {
    let maxAP = $('#actionMax').val();
    $('#remain-ap').text( maxAP);

    $('#action3').val( this.combatActions.noActionId );
    $('#action2').val( this.combatActions.noActionId );
    $('#action1').val( this.combatActions.noActionId );    
    $('#action3').prop('disabled', false);
    $('#action2').prop('disabled', false);
    $('#action1').prop('disabled', false);
  }

  /* -------------------------------------------- */
  updateAP( ) {
    let remainAP   = $('#actionMax').val();

    let action3Index = $('#action3').val();
    if ( this.combatActions.actionsList[action3Index].name != 'No Action') {
      remainAP -= 3;
    }

    let action2Index = $('#action2').val();
    if ( this.combatActions.actionsList[action2Index].name != 'No Action') {
      remainAP -= 2;
    }

    let action1Index = $('#action1').val();
    if ( this.combatActions.actionsList[action1Index].name != 'No Action') {
      remainAP -= 1;
    }

    $('#remain-ap').text( remainAP);

    if ( remainAP < 0 ) {
      $("#remain-ap").addClass("text-red");
      ui.notifications.warn("No more Action Points ! Please check your phases !");
    } else {
      $("#remain-ap").removeClass("text-red");
    }

  }
    
  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('#actionMax').change((event) => {
      this.resetAP(  );
    });
    html.find('.action-select').change((event) => {
      this.updateAP(  );
    });

  }

}
