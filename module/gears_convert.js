const gearsData = [
  {
    "name": "USD-383 “Wasp” Light Pistol",
    "type": "weapon",
    "cost": 5,
    "damage": "♠12M/♥8M/♦4M/♣2M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Range 10; Shots 10",
    "description": "A personal protection favorite, the USD-383 “Wasp” Light Pistol is ready to lock and load! Take aim at your favorite shooting range or when your life is in danger. The gun is small enough to fit most holsters or to slip into a purse! It comes with patent-pending rubberized grip and is available in gun barrel gray, matte black, racing stripe red or powder pink."
  },
  {
    "name": "USD-720 “Widowmaker” Heavy Pistol",
    "type": "weapon",
    "cost": 6,
    "damage": "♠15M/♥10M/♦5M/♣2M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 2; Range 10; Shots 10",
    "description": "The ultimate man-stopper, the USD-720 “Windowmaker” Heavy Pistol is sure to leave you standing and your enemies taking a dirt nap. A long-time favorite of bounty hunters and security forces the Sol system over, this sleek .50 caliber handgun is now available on the open market! Order yours today!"
  },
  {
    "name": "USD-1200 “Amazon” Light Rifle",
    "type": "weapon",
    "cost": 6,
    "damage": "♠18M/♥12M/♦6M/♣3M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Hands 2; Range 20; Shots 10",
    "description": "The rifle you need when you're on the go, the USD-1200 “Amazon” Light Rifle has been designed from the ground up for easy transportation and assembly. With balancing range, magazine capacity and ease of maintenance, the Amazon gets the job done. Satisfaction guaranteed. Some assembly required."
  },
  {
    "name": "USD-3200 “Valkyrie” Heavy Rifle",
    "type": "weapon",
    "cost": 7,
    "damage": "♠21M/♥14M/♦7M/♣3M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 2; Hands 2; Range 20; Shots 20",
    "description": "The USD-3200 “Valkyrie” Heavy Rifle is a masterpiece of twenty-third century engineering, combined with sleek twenty-second century ascetics. Own the best-designed and best-looking heavy rifle on the market! It comes with two autofire settings and a designer gun case. Remember, Valkyrie means quality."
  },
  {
    "name": "Xenocom X4 Machine Pistol",
    "type": "weapon",
    "cost": 9,
    "damage": "♠18M/♥12M/♦6M/♣3M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 3; Mil; Range 10; Shots 15",
    "description": "The Xenocom X4 Machine Pistol is a military favorite. Coming with the best fully automatic capability of any pistol on the market, the X4 outdoes the competition. Pick yours up and feel the ballistic power in your hand. Place an order for the entire unit. Outfit our squad with the best!"
  },
  {
    "name": "Xenocom X8 Machine Rifle",
    "type": "weapon",
    "cost": 10,
    "damage": "♠24M/♥16M/♦8M/♣4M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 3; Hands 2; Mil; Range 20; Shots 30",
    "description": "When you want maximum staying power, you want the Xenocom X8 Machine Rifle. Combining the best in range and magazine capacity with the best in full-auto heat suppression technology, the Xenocom X8 is second to none. Pair with our range finder or tracer round extras for the ultimate military battlefield kit."
  },
  {
    "name": "USD-490L “Indra” Light Laspistol",
    "type": "weapon",
    "cost": 7,
    "damage": "♠12M/♥8M/♦4M/♣2M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Range 10; Shots 5",
    "description": "In zero-G the professional soldier knows not to be caught with only a ballistic gun at hand, because dead is what she will be. Don't let this happen to you. Take the USD-490L “Indra” Light Laspistol into your zero-G battles and you will never want to use another lasgun again!"
  },
  {
    "name": "USD-840L “Horus” Heavy Laspistol",
    "type": "weapon",
    "cost": 8,
    "damage": "♠15M/♥10M/♦5M/♣2M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 2; Range 10; Shots 5",
    "description": "The USD-840L “Horus” Heavy Laspistol is simply best heavy laspistol on the market. Designed from heavy-duty metal alloys and with anti-heat ceramic plating, the Horus outshoots the competition. Plus, the Horus' state of the art battery packs are guaranteed to hold the charges for months without unwanted discharge."
  },
  {
    "name": "USD-2280L “Zeus” Light Lasrifle",
    "type": "weapon",
    "cost": 8,
    "damage": "♠18M/♥12M/♦6M/♣3M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Hands 2; Range 20; Shots 5",
    "description": "Perfect for use on spaceships or stations across the system, the USD-2280L “Zeus” Light Lasrifle is jam-packed with all the essentials needed for successful space security. From its sleek, curved barrel to its finger-molded grip, the Zeus is a miracle for the professional security worker."
  },
  {
    "name": "USD-4700L “Thor” Heavy Lasrifle",
    "type": "weapon",
    "cost": 9,
    "damage": "♠21M/♥14M/♦7M/♣3M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 2; Hands 2; Range 20; Shots 5",
    "description": "Don't be out-gunned: carry the USD-4700L “Thor” Heavy Lasrifle. The best in range, accuracy and automatic fire capacity, every Thor heavy lasrifle is constructed from the finest materials and tested by the weapons experts at Utakar System Dynamics. So don't be out-gunned. Choose the best!"
  },
  {
    "name": "Xenocom L3 Machine Laspistol",
    "type": "weapon",
    "cost": 11,
    "damage": "♠18M/♥12M/♦6M/♣3M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 3; Mil; Range 10; Shots 10",
    "description": "The L3 Machine Laspistol is the latest military-grade laspistol from Xenocom. It's heavy-duty ionized battery contains enough charge for sustained fire, and its state of the art heat-sinks support the highest rates if automatic fire. Pick yours up and make the best decision you've ever made today!"
  },
  {
    "name": "Xenocom L8 Machine Lasrifle",
    "type": "weapon",
    "cost": 12,
    "damage": "♠24M/♥16M/♦8M/♣4M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Autofire 3; Hands 2; Mil; Range 20; Shots 10",
    "description": "When in combat, it's best to remember: don't let up. That's why you need the Xenocom L8 Machine Lasrifle. With this high quality military-grade laser rifle, you can keep the pressure on, pinning your enemies down with its automatic fire and keeping your allies out of the line of fire."
  },
  {
    "name": "USD-9300 Frag Grenade",
    "type": "weapon",
    "cost": 4,
    "damage": "♠24M/♥16M/♦8M/♣4M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Area 5; Mil; Thrown",
    "description": "The USD-9300 is the ultimate in frag grenade flexibility and reliability. It comes with two modes of operation: impact or timer. Simply arm the grenade and fling it at foes, or let it count down. The choice is yours!"
  },
  {
    "name": "USD-9700 Stun Grenade",
    "type": "weapon",
    "cost": 3,
    "damage": "♠24L/♥16L/♦8L/♣4L",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Area 5; Stun; Thrown",
    "description": "Perfect for crowd control, the USD-9700 Stun Grenade is a wonder to behold. Just set the grenade to burst on timer or upon impact and let it go. The USD-9700 is guaranteed to stun or incapacitate with nonlethal precision. Order yours today!"
  },
  {
    "name": "Baton",
    "type": "weapon",
    "cost": 1,
    "damage": "♠Str×3+6L/♥Str×2+4L/♦Str+2L/♣½Str+1L",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Barons are a tried-and-true classic when it comes to dealing out blows with a minimum of lethal force. Favored as a control weapon by light security forces the world over, it never hurts to have a baton at your side. Baton telescopes for easy pocket storage. It is available in both solid colors and faux-wood."
  },
  {
    "name": "Combat Utility Knife",
    "type": "weapon",
    "cost": 1,
    "damage": "♠Str×3M/♥Str×2M/♦StrM/♣Str×½M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Thrown",
    "description": "It's a weapon. It's a tool. It's both! Combat utility knives are a versatile weapon and tool, balanced for both hand-to-hand use as well as sudden throws. Combat utility knives can be used to cut rope, pry open simple doors or butter toast. Never leave home without one! The handle comes in a variety of custom styles."
  },
  {
    "name": "Sinoex Monowhip XT",
    "type": "weapon",
    "cost": 5,
    "damage": "♠StrS/♥Str×¾S/♦Str×½S/♣Str×¼S",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Mil; Pierce 10",
    "description": "Critical Effect: When this critical effect is chosen, the monowhip does not break upon impact, as is normal. Crafted by the exotic materials experts at Sinoex, the Sinoex Monowhip XT is a coiled strand of near-monomolecular wire with a weight on the end held in a magnetized handle. Upon pressing a button, a power source in the handle causes the wire to uncoil and extend outward, providing the ideal cutting tool. Monowhips tear through armor like cardboard, but their limited tensile strength means they are typically one-use weapons, breaking after they successfully strike a target."
  },
  {
    "name": "USD-07 “Firefly” Shockstick",
    "type": "weapon",
    "cost": 4,
    "damage": "♠Str+18L/♥Str+12L/♦Str+6L/♣Str+3L",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Stun",
    "description": "Critical Effect: When this critical effect is chosen, increase the target's Stun consequence a severity. Incapacitate your opponents with a nonlethal electric charge, using this USD-07 “Firefly” Shockstick. The Firefly is a telescoping baton that has a rubberized grip on one end. This insulates the wielder from the electric charge, which can be switched on or off using a button on the grip. If struck, the electrolyzed end discharges, delivering a shock to the opponent intended to stun or incapacitate."
  },
  {
    "name": "Unarmed",
    "type": "weapon",
    "cost": "—",
    "damage": "♠Str×3L/♥Str×2L/♦StrL/♣½StrL",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "It doesn't get much more primal than old-school fisticuffs. Despite all the technological ways to go for blood, fistfights remain the most common form of violence in the Sol system. This listing represents the human capacity to attack with fists, feet or teeth."
  },
  {
    "name": "Ballistic Clips",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Mil (clips for guns with the Mil property only)",
    "description": "What are guns without clips of ammunition? Poorly-designed clubs! Don't bring a club to a gun fight. Ammunition is sold in clips fitting the gun in question. Most guns have their own standard for ammunition in terms of both caliber and cartridge design. The type of gun the ammo is designed for must be specified when the clip is purchased."
  },
  {
    "name": "Laser Batt-Packs",
    "type": "gear",
    "cost": 3,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Mil (batt-packs for guns with the Mil property only)",
    "description": "Keep your lasers powered up and ready! Laser weapons are powered by high-density batt-packs, capable of releasing the tremendous charge necessary to power weaponized lasers in a small amount of time. The downside of these high-density batt-packs is that it takes serious industrial equipment to recharge them, meaning that batt-packs are typically bought as one-use items and the expended packs simply sold back to the manufacturer for pocket change. Each batt-pack holds the weapon's listed “shots” in charges. Like clips, batt-packs are typically manufactured for a single model of firearm."
  },
  {
    "name": "USD-11h “Bulwark” Ballistic Suit",
    "type": "armor",
    "cost": 5,
    "damage": "",
    "dr": 2,
    "computer": "",
    "software": "",
    "properties": "Gel 3; Worn",
    "description": "Simply the best in light but durable ballistic protection, the USD-11h “Bulwark” Ballistic Suit stops bullets in their tracks.  Utilizing Utakar's proprietary Shell-Stopper™ gel technology, the Bulwark hardens upon impact, distributing the kinetic force of the impact throughout the body. Self-sealing gel closes in seconds. Designer pockets fit most common magazines. It is available in black, charcoal, rosewood and navy."
  },
  {
    "name": "USD-24k “Aegis” Reflect Suit",
    "type": "armor",
    "cost": 6,
    "damage": "",
    "dr": 2,
    "computer": "",
    "software": "",
    "properties": "Reflect 3; Worn",
    "description": "Do lasers threaten to give you health problems by burning holes into your flesh? Then look no further! The USD-24k “Aegis” Reflect Suit scatters lasers before they can scatter you. Constructed of bleeding-edge materials, the Aegis refracts laser light, leaving the wearer unharmed. This comes with either a frosted or glossy finish."
  },
  {
    "name": "P&W Wide-Trim Designer Coverjack",
    "type": "armor",
    "cost": 4,
    "damage": "",
    "dr": 3,
    "computer": "",
    "software": "",
    "properties": "Conceal 2; Worn",
    "description": "Just because you need to wear armor doesn't mean you have to sacrifice style. With the Pfeiffer & Wu Wide-Trim Designer Coverjack you can have your cake and eat it, too! Designed for maximum comfort and discrete protection, all P&W coverjacks can pass as normal suits upon both visual and infrared inspection. This comes in a variety of designer colors! Power tie optional."
  },
  {
    "name": "Unitech “Sunspot” C3 Vac Suit",
    "type": "armor",
    "cost": 5,
    "damage": "",
    "dr": 3,
    "computer": "",
    "software": "",
    "properties": "Big 5; Bulky 1; Vac; Worn",
    "description": "The Unitech “Sunspot” C3 Vac Suit is the spacer's best friend. Capable of operating for up to six hours in complete vacuum and in a variety of pressure, radiation and atmospheric conditions, this space suit is truly the first in its class. Compressed air canisters connect at both hips, providing redundancy and allowing changes in the field. AR link included."
  },
  {
    "name": "USD-38n “Knight” Tactical Suit",
    "type": "armor",
    "cost": 8,
    "damage": "",
    "dr": 5,
    "computer": "",
    "software": "",
    "properties": "Big 5; Bulky 1; Worn",
    "description": "Utakar's latest design in heavy civilian armor, the USD-38n “Knight” Tactical Suit employs advanced safety measures to keep the wearer up and fighting. Built from the ground up for reliable protection, the Knight Tactical Suit is a must-have for any urban warrior. Straps and pockets carry extra magazines. Comes in a variety of patterns and colors. Available in both matte and gloss."
  },
  {
    "name": "USD-44p “Shogun” Combat Armor",
    "type": "armor",
    "cost": 11,
    "damage": "",
    "dr": 10,
    "computer": "",
    "software": "",
    "properties": "Big 5; Bulky 2; Mil; Worn",
    "description": "The best protection an infantryman can get, the USD-44p “Shogun” Combat Armor is the prize of the battlefield and tested by Utakar's hands-on quality assurance experts. Remember, if you're not wearing USD, you're probably dead. Features an AR link, HUD, multiple gun holsters and clipped belt for carrying grenades. Customized paint jobs at no additional cost. Available in both matte and gloss."
  },
  {
    "name": "Xenocom “Infinity” PowerShell Armor",
    "type": "armor",
    "cost": 14,
    "damage": "",
    "dr": 20,
    "computer": "",
    "software": "",
    "properties": "Big 15; Bulky 3; Mil; Str 5; Vac; Worn",
    "description": "Go beyond the usual limits with Xenocom “Infinity” Powershell Armor. The go-to choice of security professions for more than three decades, Infinity power armor is rugged and reliable. Tailor-made for field maintenance and repair, the Infinity comes with HUD, AR link, a six-hour air supply, left and right arm weapon mounts and internal power cell. Holster points optional. Comes in a variety of colors."
  },
  {
    "name": "Adrenaline Booster Gland",
    "type": "gear",
    "cost": 9,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Implant 2; Mil",
    "description": "A boost when you need it, the adrenaline booster gland is an artificial organ implanted near the kidneys. Typically triggered through a pre-programmed biological input such as saying a specific word, a tongue pattern or a series of finger taps, the implant gives a +2 bonus to Strength, Dexterity, Speed and Endurance actions for a number of rounds equal to the target's Endurance. This bonus also applies to Strength-based damage, Defense and action order. Activating the gland is an interact action. After its primary effects wear off, the user increases her Fatigue consequence a severity, because the after-effects of the gland leave her shaking and weakened. The gland cannot be used for another 24 hours because it has to remanufacture its artificial adrenaline."
  },
  {
    "name": "AR Implant",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Implant 1",
    "description": "Tired of bulky AR glasses? Internalize your augmented reality! This state-of-the-art AR implant intercepts signals between the host's eyes and brain, injecting AR tags and other data into the visual signal. Operated through either voice or programmed eye movements, the AR implant comes complete with encrypted wireless interface and hard-wired off switch. For AR rules, see page XXX."
  },
  {
    "name": "Gun Arm Implant",
    "type": "weapon",
    "cost": 7,
    "damage": "♠12M/♥8M/♦4M/♣2M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Conceal 4; Implant 1; Range 10; Reload 2; Shots 3",
    "description": "Have your safety well in hand, or at least in arm, with this gun arm implant! Included is a concealed ballistic gun barrel and trigger mechanism, which embeds beneath the skin into the flesh of the forearm. The gun can be programmed to fire with a simple flick of the arm, giving the element of surprise. It holds three shots. Reloading requires opening a compartment in the arm and loading each bullet individually."
  },
  {
    "name": "Injector Unit",
    "type": "gear",
    "cost": 3,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Implant 1",
    "description": "Always have your drugs on hand when you need them with this injector unit! The implant is typically embedded in the thigh, and can be programmed to deliver a dose of a contained drug intravenously, all with the flip of a switch or a pre-programmed voice command. Holds three doses. Activating the injector unit is a reaction which costs no AP. The character must be capable of taking reactions."
  },
  {
    "name": "Radasil",
    "type": "gear",
    "cost": 3,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Don't let radiation get the best of you. Treat it preventatively with Radasil. If taken before radiation exposure, Radasil gives a +4 bonus to resist the effects of radiation. Its protection lasts for 8 hours. This potent medication still takes its toll on the body, however. Upon administration, the recipient gains the Fatigue consequence. Pack of 10 doses."
  },
  {
    "name": "Regenasone",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Get better faster with Regenasone. A potent healing aid, Regenasone speeds cellular regeneration within the body. If administered once a day, a dose of Regenasone reduces the time it takes to heal wounds from one week down to one day. This, however, leaves the recipient feeling hazy and lethargic. After Regenasone is administered and for the day afterward, the recipient increases her Fatigue consequence a severity. Pack of 10 doses."
  },
  {
    "name": "Somnitol",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Prescribed for even the toughest insomnia, Somnitol will put you to sleep. If injected, Somnitol will put the recipient into a deep sleep in a number of rounds equal to her Endurance. Once asleep, she will be unconscious for the next 8 hours unless awoken through a stimulant or by other chemical means. Pack of 10 doses."
  },
  {
    "name": "Stimulox",
    "type": "gear",
    "cost": 3,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Stimulox is recommended by doctors system-wide! Use of this drug instantly awakens an unconscious individual, granting a +1 bonus to all Strength, Dexterity and Speed actions for the next 10 minutes, as well as a +1 bonus to Strength-based damage, Defense and Shock threshold. After this effect wears off, the recipient increases her Fatigue consequence a severity. Pack of 10 doses."
  },
  {
    "name": "Torpestat",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Be the boss of pain. Don't let pain be the boss of you. Torpestat is a powerful pain-killer. Once injected, it lasts for 8 hours. During this time, the recipient does not take penalties from the Wound consequence. Additionally, she gains a +2 bonus to her Shock threshold. The downside is that the drug also leaves her numb, giving her a -2 penalty to all Dexterity and Speed actions, as well as to any Perception action relying on her sense of touch. Pack of 10 doses."
  },
  {
    "name": "Verazine",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Mil; Neg",
    "description": "When the perp won't talk, try Verazine! Verazine is a psychoactive drug used in interrogations and for even less savory purposes. It causes the recipient to enter a hazy, dream-like state, becoming talkative and losing her internal filters. Injected directly into the bloodstream, Verazine takes about a minute to set in. Then, for the next 10 minutes, the recipient is at a -4 penalty to all actions to deceive or avoid simple mental trickery. Pack of 10 doses."
  },
  {
    "name": "Minor Surgery",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "",
    "description": "Suffering from a simple bone fracture, minor organ damage or moderate tissue damage? A variety of minor surgeries may be able to help you! They are capable of removing a variety of lasting consequences, including many on the Lingering Injury tables (see page XXX). For full rules on surgery, see page XXX."
  },
  {
    "name": "Major Surgery",
    "type": "gear",
    "cost": 7,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "",
    "description": "Minor surgery not enough for your wounds? Major surgery may be just what the doctor ordered! Major surgery is capable of removing even more potent lasting consequences, including many of the worst lingering injuries (see page XXX). For full rules on surgery, see page XXX."
  },
  {
    "name": "Regrowth Therapy",
    "type": "gear",
    "cost": 8,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "",
    "description": "Need a new arm, leg or bodily organ? Look no further! Lost limbs, digits, eyes and many other organs can be regrown, but this is expensive, takes time and requires frequent visits to the doctor to apply the next stage of the therapy. Limbs in the process of regrowth typically are tender and itch fiercely. For full rules on surgery and regrowth therapy, see page XXX."
  },
  {
    "name": "Biomonitor",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Implant 0",
    "description": "This biomonitor is a small, barely-invasive implant that periodically records simple medical data, such as blood pressure, pulse, heart rate and blood sugar. It comes complete with a small external switch that turns on the capability to communicate wirelessly with medical equipment. This aids in diagnosis and first aid on the host, granting a +2 bonus to such endeavors."
  },
  {
    "name": "Cybernetic Replacement Limb",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Implant 1",
    "description": "Can't afford the cost of a regrowth treatment? This cybernetic limb has you covered! Just have the nerve interface installed on the arm or leg's stump and let the machinery do the rest. A week's training is required to regain full limb capability. Cybernetic arms and legs typically come complete with faux-skin, making them look slightly less unnatural. A cybernetic limb will never have tactile input like a natural arm or leg, but in all other regards, cybernetic limbs function like the real thing."
  },
  {
    "name": "Disposable Test Kit",
    "type": "gear",
    "cost": 3,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Have a specific condition that needs testing? That calls for a disposable test kit. This kit tests for a specific disease, toxin, drug or medical condition, such as sleep deprivation or pregnancy. It is typically administered through either drawing blood or through a urine sample. Results usually take a minute or two. Contains 10 disposable tests."
  },
  {
    "name": "Medikit (Basic)",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Treat nagging injuries on the go! The medikit contains basic medical tools, useful for treating all major or simple injuries, including adhesive spray, antibiotics, a variety of ointments, an oxygen mask and sutures. This counts as sufficient tools for first aid or improvised equipment (-2) for minor surgery. Also, consider our superior quality medikits, which count as superior tools (+2) to first aid! Contains 10 uses."
  },
  {
    "name": "Medikit (Superior)",
    "type": "gear",
    "cost": "",
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Treat nagging injuries on the go! The medikit contains basic medical tools, useful for treating all major or simple injuries, including adhesive spray, antibiotics, a variety of ointments, an oxygen mask and sutures. This counts as sufficient tools for first aid or improvised equipment (-2) for minor surgery. Also, consider our superior quality medikits, which count as superior tools (+2) to first aid! Contains 10 uses."
  },
  {
    "name": "Surgery Kit (Basic)",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": 1,
    "software": "",
    "properties": "—",
    "description": "Not all injuries happen within easy reach of a hospital or medical bay. For unexpected injuries in the remote reaches of space, there's the portable surgery kit. With a portable pack of surgeon's tools, diagnostic gear, cheap anesthetics and antibiotics, this kit counts as sufficient equipment for minor surgery, or improvised equipment (-2) for major surgery. Also consider our superior quality surgery kits, which count as superior tools (+2) for minor surgery or sufficient tools for major surgery! Contains materials for 10 surgeries. Performing surgery using one of these kits does not incur the expense of paying for a full surgery treatment (see page XXX)."
  },
  {
    "name": "Surgery Kit (Superior)",
    "type": "gear",
    "cost": 9,
    "damage": "",
    "dr": "",
    "computer": 1,
    "software": "",
    "properties": "—",
    "description": "Not all injuries happen within easy reach of a hospital or medical bay. For unexpected injuries in the remote reaches of space, there's the portable surgery kit. With a portable pack of surgeon's tools, diagnostic gear, cheap anesthetics and antibiotics, this kit counts as sufficient equipment for minor surgery, or improvised equipment (-2) for major surgery. Also consider our superior quality surgery kits, which count as superior tools (+2) for minor surgery or sufficient tools for major surgery! Contains materials for 10 surgeries. Performing surgery using one of these kits does not incur the expense of paying for a full surgery treatment (see page XXX)."
  },
  {
    "name": "Microcomputer",
    "type": "gear",
    "cost": 1,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Are you tracking an object, calculating basic numbers or broadcasting a message on loop? Do you need only very basic computing and wireless ability? Then a microcomputer is what you're looking for! This dime-sized computer is capable of basic computations and processing. It has inbuilt wireless access using either IR or radio comms (see page XXX). Unlike other computers and due to its size, it has no display or other input or output features, requiring peripherals or wireless communication with another device."
  },
  {
    "name": "Hand Terminal",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": 3,
    "software": "",
    "properties": "Neg",
    "description": "The ubiquitous hand terminal is a must-have for anyone! These handheld computers work as video and audio comms; are capable of IR and wireless connections; have an inbuilt camera, microphone and GPS; play games; do basic calculations; connect to the net; and interface with most common AR implants and glasses. Most come with both voice and touchscreen controls."
  },
  {
    "name": "Workstation",
    "type": "gear",
    "cost": 6,
    "damage": "",
    "dr": "",
    "computer": 5,
    "software": "",
    "properties": "Big 5",
    "description": "For serious computing needs, a workstation computer will excel! These computers are designed to be set on a desk or hung on a wall. They have the serious processing power needed for the most resource intensive games, extended scientific calculations and encryption. They can also serve as a makeshift server if needed. Workstation computers typically come with touchscreen and voice controls, as well as key inputs and sometimes simple biometric input."
  },
  {
    "name": "Mainframe",
    "type": "gear",
    "cost": 9,
    "damage": "",
    "dr": "",
    "computer": 7,
    "software": "",
    "properties": "Big 10",
    "description": "Do you have the need for not just sufficient, but vigorous computing? Order a mainframe today! Mainframes are designed to go beyond personal computing needs. They host popular net nodes, serve as the main computers for spaceships, administrate corp sites and sit on academic campuses. Mainframes are generally large machines meant to remain wired in place and communicated with through individual workstations or hand terminals. Many have wireless capabilities, but this is often left turned off to take advantage of the superior bandwidth offered by wired connections."
  },
  {
    "name": "Supercomputer",
    "type": "gear",
    "cost": 13,
    "damage": "",
    "dr": "",
    "computer": 9,
    "software": "",
    "properties": "Big 20",
    "description": "Some of the best computers that money can buy, supercomputers are typically found only in the most important corp offices, used in the most computationally intensive academic problems or backing the largest of the large net nodes. They are almost always designed to interface with personal hand terminals or workstations rather than with users directly. While supercomputers can be ported around in theory, they actually require extensive setup before they are operational."
  },
  {
    "name": "Encryption System (Basic)",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 3,
    "properties": "—",
    "description": "Keep your private life private. Encryption is necessary for any sort of secure transaction using broadcast communication, such as on the net or through radio comms. Most computers come with basic built-in encryption methods, but for those who want real security, bolstered encryption software is a no-brainer. Difficulty of breaking encryption scales with processing requirements. Encryption systems come in a variety of different qualities, with differing processing requirements (see page XXX)."
  },
  {
    "name": "Encryption System (Pro)",
    "type": "gear",
    "cost": 6,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 5,
    "properties": "—",
    "description": "Keep your private life private. Encryption is necessary for any sort of secure transaction using broadcast communication, such as on the net or through radio comms. Most computers come with basic built-in encryption methods, but for those who want real security, bolstered encryption software is a no-brainer. Difficulty of breaking encryption scales with processing requirements. Encryption systems come in a variety of different qualities, with differing processing requirements (see page XXX)."
  },
  {
    "name": "Encryption System (Deluxe)",
    "type": "gear",
    "cost": 8,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 7,
    "properties": "—",
    "description": "Keep your private life private. Encryption is necessary for any sort of secure transaction using broadcast communication, such as on the net or through radio comms. Most computers come with basic built-in encryption methods, but for those who want real security, bolstered encryption software is a no-brainer. Difficulty of breaking encryption scales with processing requirements. Encryption systems come in a variety of different qualities, with differing processing requirements (see page XXX)."
  },
  {
    "name": "Encryption System (Ultimate)",
    "type": "gear",
    "cost": 10,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 9,
    "properties": "—",
    "description": "Keep your private life private. Encryption is necessary for any sort of secure transaction using broadcast communication, such as on the net or through radio comms. Most computers come with basic built-in encryption methods, but for those who want real security, bolstered encryption software is a no-brainer. Difficulty of breaking encryption scales with processing requirements. Encryption systems come in a variety of different qualities, with differing processing requirements (see page XXX)."
  },
  {
    "name": "Expert System (Basic)",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 3,
    "properties": "—",
    "description": "Know your knowns and your known unknowns! An expert system is software that provides a database of knowledge and useful advice on a given topic. This is often designed to hook in to AR software, projecting useful tags and other information right before the user's eyes, aiding in a variety of tasks. Every expert software is designed to support one skill, or sometimes a specific experience in a skill. By taking a moment (an interact action in combat) the user can query the expert system for help on this skill. Mechanically this works as if the expert system is making a group effort action supporting the user, with a total based on the quality of the software (see page XXX). An action may only benefit from one expert system at a time. Expert software comes in four qualities: basic (total 10), pro (total 12), deluxe (total 14) and ultimate (total 16). Example: Stan is using a pro expert system to help him with ship repairs. He makes the required Dex/mechanic flip and gets a total of 9, not yet taking into account the expert system. Since expert systems work exactly like secondary characters in a group effort action, and since the expert system's total—12 for pro quality—is higher than Stan's total, he gets a +2 bonus to his action. This gives him a final total of 11."
  },
  {
    "name": "Expert System (Pro)",
    "type": "gear",
    "cost": 6,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 5,
    "properties": "—",
    "description": "Know your knowns and your known unknowns! An expert system is software that provides a database of knowledge and useful advice on a given topic. This is often designed to hook in to AR software, projecting useful tags and other information right before the user's eyes, aiding in a variety of tasks. Every expert software is designed to support one skill, or sometimes a specific experience in a skill. By taking a moment (an interact action in combat) the user can query the expert system for help on this skill. Mechanically this works as if the expert system is making a group effort action supporting the user, with a total based on the quality of the software (see page XXX). An action may only benefit from one expert system at a time. Expert software comes in four qualities: basic (total 10), pro (total 12), deluxe (total 14) and ultimate (total 16). Example: Stan is using a pro expert system to help him with ship repairs. He makes the required Dex/mechanic flip and gets a total of 9, not yet taking into account the expert system. Since expert systems work exactly like secondary characters in a group effort action, and since the expert system's total—12 for pro quality—is higher than Stan's total, he gets a +2 bonus to his action. This gives him a final total of 11."
  },
  {
    "name": "Expert System (Deluxe)",
    "type": "gear",
    "cost": 8,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 7,
    "properties": "—",
    "description": "Know your knowns and your known unknowns! An expert system is software that provides a database of knowledge and useful advice on a given topic. This is often designed to hook in to AR software, projecting useful tags and other information right before the user's eyes, aiding in a variety of tasks. Every expert software is designed to support one skill, or sometimes a specific experience in a skill. By taking a moment (an interact action in combat) the user can query the expert system for help on this skill. Mechanically this works as if the expert system is making a group effort action supporting the user, with a total based on the quality of the software (see page XXX). An action may only benefit from one expert system at a time. Expert software comes in four qualities: basic (total 10), pro (total 12), deluxe (total 14) and ultimate (total 16). Example: Stan is using a pro expert system to help him with ship repairs. He makes the required Dex/mechanic flip and gets a total of 9, not yet taking into account the expert system. Since expert systems work exactly like secondary characters in a group effort action, and since the expert system's total—12 for pro quality—is higher than Stan's total, he gets a +2 bonus to his action. This gives him a final total of 11."
  },
  {
    "name": "Expert System (Ultimate)",
    "type": "gear",
    "cost": 10,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 9,
    "properties": "—",
    "description": "Know your knowns and your known unknowns! An expert system is software that provides a database of knowledge and useful advice on a given topic. This is often designed to hook in to AR software, projecting useful tags and other information right before the user's eyes, aiding in a variety of tasks. Every expert software is designed to support one skill, or sometimes a specific experience in a skill. By taking a moment (an interact action in combat) the user can query the expert system for help on this skill. Mechanically this works as if the expert system is making a group effort action supporting the user, with a total based on the quality of the software (see page XXX). An action may only benefit from one expert system at a time. Expert software comes in four qualities: basic (total 10), pro (total 12), deluxe (total 14) and ultimate (total 16). Example: Stan is using a pro expert system to help him with ship repairs. He makes the required Dex/mechanic flip and gets a total of 9, not yet taking into account the expert system. Since expert systems work exactly like secondary characters in a group effort action, and since the expert system's total—12 for pro quality—is higher than Stan's total, he gets a +2 bonus to his action. This gives him a final total of 11."
  },
  {
    "name": "Personal Assistant",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 5,
    "properties": "—",
    "description": "When you need a little bit of help, get a personal assistant! Personal assistant software uses adaptive algorithms to learn the wants, needs, schedule and habits of a particular user, looking up information and making suggestions without prompting. All personal assistant software requires several weeks to adapt to a new user, and most is given a customizable anthropomorphic avatar, or at least a name. This avatar can be projected into the surrounding environment when viewed through AR. Many personal assistants have adjustable personalities as well."
  },
  {
    "name": "Translation Software",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": 3,
    "properties": "—",
    "description": "Don't let the language barrier be an issue. Translation software provides real time on-the-fly translation between two languages. Every language pairing is its own software. Most translation software is capable of outputting the translation in both speech and text, and often includes the ability to manually enter new vocabulary and idioms. No translation software is perfect, and most still has problems translating less common subculture slang or figures of speech."
  },
  {
    "name": "AR Glasses",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Worn",
    "description": "Join the AR revolution. Join the twenty-third century. AR glasses look a lot like typical glasses, except they project augmented reality (AR) information before the wearer's eyes. This allows the wearer to read AR tags, access AR data and overlay projections on the world around her. Doing this, however, requires wirelessly interfacing the AR glasses with a computer—typically a hand terminal. This connection is typically a radio or infrared (IR) connection."
  },
  {
    "name": "Biometric Reader",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "For all your secure needs, you can rely on a biometric reader. A biometric reader is typically used as a secure input device to authenticate the identity of the computer user. Most biometric readers scan fingerprints or retinas, although DNA biometric readers exist as well. A biometric reader is typically connected to a host computer, which then takes input from the device."
  },
  {
    "name": "Data Chip",
    "type": "gear",
    "cost": -1,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Data chips: for when the net just won't cut it! Data chips are small silicate chips intended to transfer data physically between computers. They are often used when transferal over the net would be deemed insecure or where no local net is available. All data chips have a software rating. While data chips cannot run software as can a computer, they can store software up to the same rating. So, for example, a Data Chip 5 could store a Software 5 program or two Software 4 programs, etc. A data chip has a cost rating equal to the software rating."
  },
  {
    "name": "IR Comm",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Does your device not speak infrared? Get a dedicated IR comm! IR comms use infrared to transmit and receive data. This is useful only at very short ranges—usually no more than a few dozen meters. It is of particular use in short distance, secured transmissions because IR does not penetrate walls and its short distance limits eavesdropping. IR communication comes built in to most hand terminals and other short-range wireless devices. A dedicated IR comm, such as this one, can be attached to other devices to give IR communication capabilities."
  },
  {
    "name": "Laser Comm",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Direct your communication with a laser comm. Laser comms use lasers to transmit and receive data at a directed target. This limits the ability to intercept the data because it is not broadcast to the world but rather directed at a specific target. Naturally, the ability to use a laser comm relies on having a direct line of communication unobscured by walls, the curve of planetary bodies or atmospheric interference. This personal laser comm is good for a dozen kilometers on a clear day in atmospheric conditions or several thousand kilometers in the vacuum of space."
  },
  {
    "name": "Radio Comm",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Radio comms are the most common type of comms in the solar system. They broadcast out radio waves in all directions, carrying data outward. Some wavelengths even have the ability to bounce off walls and atmospheric conditions, spreading or directing their signal in different ways. This maximizes the distance of communication at the expense of being easy to intercept. This personal dedicated radio comm can transmit up to a range of several thousand kilometers."
  },
  {
    "name": "Binoculars",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Don't let your enemies get close; spot them from afar with these binoculars! Binoculars are a tried and true method of seeing things at large distances. These binoculars are good for ×4 magnification, giving a +2 bonus to see objects at great distances."
  },
  {
    "name": "Camera",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Be a pro photographer! Although most hand terminals come complete with a camera, this dedicated camera far surpasses the typical hand terminal camera in both speed, resolution and assorted options. Good for both still shots and video recording! Net-capable for immediate uploading. Apply a variety of filters on the go."
  },
  {
    "name": "Chem-Sniff",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Sniff out trouble with this chem-sniff. This olfactory sensor is able to detect and recognize substances by smell, comparing them against a database of commonly known scents. While it is not useful in a vacuum, underwater or against anything in a sealed environment, most everything otherwise leaves an olfactory signature that can be recorded and recognized again."
  },
  {
    "name": "Geiger-Counter",
    "type": "gear",
    "cost": 3,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Worn",
    "description": "A trusted method of detecting radiation, this clip-on geiger-counter will also determine the intensity of the radiation exposure and keep a count of absorbed rads. It can be set to alert the wearer either visually through its integrated display or audibly with a series of clicking noises."
  },
  {
    "name": "Ladar Scanner",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Ladar! Ladar! Ladar! Ladar scanners emit low-energy laser beams to detect and identify nearby objects. Although they are of limited use in detecting unknown objects due to the narrowness of their beams, they have remarkable resolution in identifying objects once they are known. This includes facial recognition and chemical recognition technologies! Just compare to a facial database, or to a database of chemicals based on which wavelengths of the laser light the materials absorb. Signal is good up to a dozen kilometers in atmosphere or a thousand kilometers in a vacuum."
  },
  {
    "name": "Night Vision Glasses",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Worn",
    "description": "Don't be caught in the dark without them. They look like regular sunglasses, meeting all your style requirements! These night vision glasses can detect infrared, projecting a false-color image right before the wearer's eyes. Good for seeing objects in the dark but less useful for reading or color detection."
  },
  {
    "name": "Biometric Cracker",
    "type": "gear",
    "cost": 7,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "This device quickly cycles through biometric input used to open locks or secure areas. It is capable of imitating the input for fingerprints, irises, DNA and other common biometric methods. Wiring this device first into the biometric reader is required because it only fakes biometric data in digital form. It does not fake actual physical fingers or irises. This device counts as sufficient tools when surpassing such a device and is typically capable of bypassing compatible devices in a matter of seconds."
  },
  {
    "name": "Chameleon Suit",
    "type": "gear",
    "cost": 7,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Bulky 1; Worn",
    "description": "You'll blend right in with this chameleon suit! This tight-fitting suit changes color as the wearer moves, similar to a chameleon. This aids the wearer's attempts at stealth, but it does not fool infrared detection. It counts as a superior tool (+2) for stealth against simple, unaided observation."
  },
  {
    "name": "Disposable Cuffs",
    "type": "gear",
    "cost": 3,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "These disposable cuffs are little more than metal-meshed tape that can automatically tighten over the limbs of their target once applied. Further struggles against the cuffs cause them to tighten further. Each length is good for one use. Comes with 10 uses."
  },
  {
    "name": "E-Lockpick",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Never be sealed out of electronic doors again! This small device cycles rapidly through possible electronic lock combinations and patterns, gaining access to electronically-sealed doors or hatches. This counts as sufficient tools for actions to open electronic locks and typically opens them within a few seconds. Attempts to access the locks may still be logged by locks capable of doing so."
  },
  {
    "name": "IR Cloak",
    "type": "gear",
    "cost": 7,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Bulky 1; Worn",
    "description": "Do your enemies have the means of IR detection? Foil even that with this IR cloak! An IR cloak appears to be a thick poncho or tarp, which is usually matte black in color. It self-adjusts to the ambient temperature to fool infrared detection methods. This is usually coupled by cutting the lights or otherwise moving around in the dark to foil visible spectrum detection as well."
  },
  {
    "name": "Magnetic Grapnel",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Get to unreached heights fast. Try a magnetic grapnel today! This coiled grapnel gun can almost silently fire a magnetic grapnel, with a cord up to 30 meters. It can then be set to recoil itself, pulling the user towards its target at the rate of 5 meters per round. Hitting the target is a Dex/guns flip. The cord can safely hold up to 200 kg. If trying to load the cord with more than this, have the GM flip a card. If it’s a club, the cord breaks. Flip an additional card for every increment of 200 kg."
  },
  {
    "name": "Disguise Kit (Basic)",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Be who you want to be with this disguise kit! This complete kit contains makeup, chemicals for mimicking muscle twitches and tightness, prosthetic attachments, instant hair coloring and colored contacts. This counts as sufficient tools for disguise. Also consider our superior quality version, which counts as superior tools (+2) for the purposes of disguise! Each kit contains enough materials for 10 uses."
  },
  {
    "name": "Disguise Kit (Superior)",
    "type": "gear",
    "cost": 9,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Be who you want to be with this disguise kit! This complete kit contains makeup, chemicals for mimicking muscle twitches and tightness, prosthetic attachments, instant hair coloring and colored contacts. This counts as sufficient tools for disguise. Also consider our superior quality version, which counts as superior tools (+2) for the purposes of disguise! Each kit contains enough materials for 10 uses."
  },
  {
    "name": "Microbug",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Never miss a hidden event: use a microbug! A microbug is a flea-sized recording device that captures both audio and video input. It can store up to 10 minutes of input on the device itself, but typically transmits its recording wirelessly to a remote computer. Transmission can be to occur in short bursts, in a constant stream or at a specified time."
  },
  {
    "name": "Voice Mask",
    "type": "gear",
    "cost": 4,
    "damage": "♠18M/♥12M/♦6M/♣3M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Studies show that the largest cause of disguise failure is identification by voice. Change your voice today! This device attaches to the wearer's throat, usually sitting on the shoulder or neck, beneath the collar. Once attached and turned on, it will distort the wearer's voice so as to be unrecognizable. Other people's voices can be mimicked, but this requires both a sample audio recording of the speaker to be mimicked, and calibration with the wearer's voice—an Int/programming-10 flip.  (Improvised)"
  },
  {
    "name": "Demolition Charge",
    "type": "gear",
    "cost": 4,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Area 5; Pierce 3",
    "description": "Useful in mining, boarding actions or in construction, demolition charges are a must-buy! Simply place the charge, activate the receiver, evacuate the area and give the demolition signal wirelessly. Also, this is useful as an improvised explosive weapon! Please follow basic safety precautions when setting demolition charges."
  },
  {
    "name": "Plasma Cutter",
    "type": "weapon",
    "cost": 6,
    "damage": "♠9M/♥6M/♦3M/♣1M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Pierce 3",
    "description": "A favorite in heavy duty construction work, ship repair and emergency boarding actions, this plasma cutter cuts right through even the toughest metals and ceramics. The airlock busted? Just cut right through the hull! A plasma cutter can also be used as an improvised weapon in a pinch (see the damage listing above). Count as an improvised weapon."
  },
  {
    "name": "Power Tool",
    "type": "weapon",
    "cost": 4,
    "damage": "♠Str×3M/♥Str×2M/♦StrM/♣Str×½M",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Some repair and constructions tasks require more than a simple toolkit. For those, try a dedicated power tool! Power tools come in a variety of shapes and sizes from chainsaws to magnetic drills. In a pinch, they can even serve as an improvised hand-to-hand weapon! (See the damage listing above.). Count as an improvised weapon."
  },
  {
    "name": "ToolKit (Basic)",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "After using our toolkits, you'll feel naked without one! A variety of toolkits are available, each geared towards a specific kind of task and each containing tools useful in that task. When purchasing a toolkit, think about what the toolkit is geared for as a specific skill experience. For example, “electrician” might be a toolkit used with the mechanic skill. Tool kits count as sufficient equipment for that specialty and count as improvised equipment for other uses of the same skill. Also, consider our top of the line superior toolkits for the best tools that money can buy. These superior toolkits count as superior tools (+2) for the specialty in question and sufficient tools for any other application of the skill."
  },
  {
    "name": "ToolKit (Superior)",
    "type": "gear",
    "cost": 9,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "After using our toolkits, you'll feel naked without one! A variety of toolkits are available, each geared towards a specific kind of task and each containing tools useful in that task. When purchasing a toolkit, think about what the toolkit is geared for as a specific skill experience. For example, “electrician” might be a toolkit used with the mechanic skill. Tool kits count as sufficient equipment for that specialty and count as improvised equipment for other uses of the same skill. Also, consider our top of the line superior toolkits for the best tools that money can buy. These superior toolkits count as superior tools (+2) for the specialty in question and sufficient tools for any other application of the skill."
  },
  {
    "name": "Backpack",
    "type": "container",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Bulky 1; Container 5; Worn",
    "description": "Strap things to your back so that you don't have to carry them in your hands! This designer backpack comes in a variety of colors and styles—perfect for any occasion. The backpack features one large pocket and a variety of smaller pockets for your carrying convenience. Retrieving an item from a backpack requires an interact action to open the backpack and fish around in addition to the usual interact action to retrieve the item."
  },
  {
    "name": "Flashlight",
    "type": "gear",
    "cost": 1,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "While most hand terminals have basic flashlight functionality, they don't have the power of illumination possessed by this durable, high-output survival flashlight! Useful for exploration when the power has gone out, the visible light beam illuminates at 3000 lumens and up to 500 m away!"
  },
  {
    "name": "Gecko-Climbers",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Have a surface you need to scale? Try some Gecko-Climbers! These gloves and boots strap firmly to the limbs, allowing the climber to stick to most surfaces in a manner similar to a gecko. Only capable of supporting up to 100 kg, a wearer of Gecko-Climbers should travel light. Gecko-Climbers count as sufficient tools for climbing and furthermore provide a +2 bonus on actions to climb surfaces to which they stick."
  },
  {
    "name": "GPS Tracker",
    "type": "gear",
    "cost": 2,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "Neg",
    "description": "Always know your way around with this GPS tracker! Useful on any planetary body with GPS satellites, this simple GPS tracker has a small display for its coordinates and also can transmit wirelessly to any close computer or other wireless device. It is best used with an Expert System for planetary navigation!"
  },
  {
    "name": "Rope",
    "type": "gear",
    "cost": 1,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "You never know when you'll need some good, old-fashioned rope! This rope is made of light, but durable synthetic fabrics and coils into an easy-to-transport bundle. Contained is 50 m of rope, capable of safely holding up to 200 kg. If trying to load the rope with more than this, have the GM flip a card. If it's a clubs the rope breaks. Flip an additional card for every increment of 200 kg. Rope counts as a sufficient tool for many purposes, such as climbing heights or tying someone up."
  },
  {
    "name": "Survival Kit (Basic)",
    "type": "gear",
    "cost": 5,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Never get caught in the great outdoors without one! This robust kit contains water purification tablets, a set of disposable tests for food safety, a variety of ointments, food tablets, a small knife, a lighter, a signal flare and other useful odds and ends. It counts as sufficient tools for surviving in the wild, provided the user already has an air supply. Also consider our superior quality survival kits, which count as superior tools (+2). Contains 10 uses. Each use is good for one person for one day."
  },
  {
    "name": "Survival Kit (Superior)",
    "type": "gear",
    "cost": 9,
    "damage": "",
    "dr": "",
    "computer": "",
    "software": "",
    "properties": "—",
    "description": "Never get caught in the great outdoors without one! This robust kit contains water purification tablets, a set of disposable tests for food safety, a variety of ointments, food tablets, a small knife, a lighter, a signal flare and other useful odds and ends. It counts as sufficient tools for surviving in the wild, provided the user already has an air supply. Also consider our superior quality survival kits, which count as superior tools (+2). Contains 10 uses. Each use is good for one person for one day."
  }
];
const fulltemplate =  {   "area": 0,
                        "autofire": 0,
                        "damage_club": "",
                        "damage_hearts": "",
                        "damage_spade": "",
                        "damage_diamond": "",
                        "hands": 1,
                        "pierce": 0,
                        "range": 0,
                        "reload": 1,
                        "shots": 0,
                        "stun": false,
                        "thrown": false,
                        "reach": false,
                        "shallow": false,
                        "spread": false,
                        "category": "", 
                        "equiped": false,
                        "bulky": 0,
                        "dr": 0,
                        "gel": 0,
                        "reflect": 0,
                        "str": 0,
                        "vac": false,
                        "container": 0,
                        "big": 0,
                        "computer": 0,
                        "conceal": 0,
                        "container": 0,
                        "implant": 0,
                        "neg": false,
                        "mil": false,
                        "software": 0,
                        "worn": false,
                        "description": "",
                        "quantity": 0,
                        "costrating": 0,
                        "defensive": 0,
                        "containerid": ""                  
                    }
                    
const imgWeapon = 'systems/foundryvtt-shadows-over-sol/img/icons/weapons/generic_weapon.svg'
const imgArmor = 'systems/foundryvtt-shadows-over-sol/img/icons/armors/generic_armor.svg'
const imgSoftware = 'systems/foundryvtt-shadows-over-sol/img/icons/gears/generic_software.svg'
const imgHardware = 'systems/foundryvtt-shadows-over-sol/img/icons/gears/generic_computer.svg'
const imgGear = 'systems/foundryvtt-shadows-over-sol/img/icons/gears/generic_gear.svg'

export class gearConverter {
  
  static importgear() {

    let itemfolder;
    for( let folder of game.folders.entities ) {
      if (folder.name == 'Gears')
        itemfolder = folder;
    }
    console.log("Folder", itemfolder);

    for (let gear of gearsData) {
      let item = {
        name: gear.name,
        type: gear.type,
        folder: itemfolder.data._id,
        img: imgGear,
        data: {
          costrating: gear.cost,
          description: gear.description
        }
      }
      if ( gear.type == 'weapon' ) {
        item.img = imgWeapon;
        let damages = gear.damage.match( /.(.+)([LMSC])\/.(.+)([LMSC])\/.(.+)([LMSC])\/.(.+)([LMSC])/i )
        item.data.damage_spade = damages[1] + damages[2];
        item.data.damage_hearts = damages[3] + damages[4];
        item.data.damage_diamond = damages[5] + damages[6];
        item.data.damage_club = damages[7] + damages[8];                
      }
      if ( gear.computer != "" ) {
        item.data.computer = Number(gear.computer);
        item.img = imgHardware;
      }
      if ( gear.software != "" ) {
        item.data.software = Number(gear.software);
        item.img = imgSoftware;
      }
      if ( gear.dr != "" ) item.data.dr = Number(gear.dr);
      if ( gear.type == 'armor' ) item.img = imgArmor;

      let properties = gear.properties.split(";");
      for (let property of properties) {
        property = property.trim();
        let subdata = property.split(" ");
        let fieldName = subdata[0].toLowerCase().trim();
        if ( fulltemplate[fieldName] != undefined ) {
          item.data[fieldName] = (subdata[1]) ? Number(subdata[1]) : true;
        } else {
          console.log("Property not found!", subdata[0]);
        }
      }
      Item.create( item );
      console.log("ADDED : ", item);
    }
  }
}