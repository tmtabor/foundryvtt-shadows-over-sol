 html.find('.item .item-name h4').click(event => this._onItemSummary(event));
 
  /**
   * Handle toggling of an item from the Actor sheet
   * @private
   */
  _onItemSummary(event) {
    event.preventDefault();
    let li = $(event.currentTarget).parents(".item"),
        item = this.actor.getOwnedItem(li.data("item-id"));

    // Toggle summary
    if (item.data.data.description !== undefined && item.data.data.description !== null){
      if ( li.hasClass("expanded") ) {
        let summary = li.children(".item-summary");
        summary.slideUp(200, () => summary.remove());
      } else {
        let div = $(`<div class="item-summary">${item.data.data.description}</div>`);
        li.append(div.hide());
        div.slideDown(200);
      }
      li.toggleClass("expanded");
    }
  }