/* -------------------------------------------- */
import { SoSUtility } from "./sos-utility.js";

/* -------------------------------------------- */
export class SoSFlipDialog extends Dialog {

  /* -------------------------------------------- */
  constructor(flipData, html) {
    let conf = {
      title: 'Flip Dialog',
      content: html,
      buttons: {
        'flip-close': { label: 'Cancel and Close', callback: html => this.onFlipClose() }
      },
      default: 'flip'
    };
    super(conf, { classes: ["sosdialog"], width: 800  });
  
    this.flipData = flipData;
  }

  /* -------------------------------------------- */
  onFlipClose( ) {
    this.close();
  }

  /* -------------------------------------------- */
  updateScoreBase( ) {
    let scoreBase = 0;
    if ( this.flipData.mode == 'skill' || this.flipData.mode == 'weapon' ) {
      let statKey = $('#statSelect').val();
      this.flipData.stat = duplicate( this.flipData.statList[ statKey ] );
      scoreBase = Math.floor(this.flipData.statList[ statKey ].value / 2) + this.flipData.skill.data.value;
    } else { //Stat mode
      let statKey = $('#statSelect').val();
      scoreBase = this.flipData.stat.value;  
    }
    this.flipData.woundMalus = 0;
    this.flipData.woundMalus += ($('#wound-light-checkbox').is(":checked")) ? (-1) : 0;
    this.flipData.woundMalus += ($('#wound-moderate-checkbox').is(":checked")) ? (-2) : 0;
    this.flipData.woundMalus += ($('#wound-severe-checkbox').is(":checked")) ? (-3) : 0;
    this.flipData.woundMalus += ($('#wound-critical-checkbox').is(":checked")) ? (-4) : 0;
    scoreBase += this.flipData.malusConsequence;
    scoreBase += this.flipData.bonusConsequence;
    scoreBase += this.flipData.woundMalus;
    $('#wound-malus').text(this.flipData.woundMalus);
    $('#score-base').text( scoreBase);
  }

  /* -------------------------------------------- */
  async updateFlip( flipData ) {
    //console.log("UPDATE !!!", flipData);
    $('.view-deck').remove();
    $("#view-deck").append(await flipData.actor.cardDeck.getDeckHTML());

    $('.view-edge').remove();
    $("#view-edge").append(await flipData.actor.cardDeck.getEdgeHTMLForFlip());

    this.updateScoreBase();

    $('.edge-card').click((event) => {
      let flipData = this.flipData;
      flipData.modifier = $('#modifier').val();
      flipData.tn = (flipData.target) ? flipData.target.actor.data.data.scores.defense.value : $('#tn').val();
      flipData.edgeName = event.currentTarget.attributes['data-edge-card'].value;
      flipData.edgeLuck = $('#edge-luck').is(":checked");
      flipData.cardOrigin = "Edge";
      if ( flipData.mode == 'skill' || flipData.mode == 'weapon') {
        flipData.stat = duplicate( flipData.statList[ $('#statSelect').val() ] );
      }
      console.log("CLICK:", flipData);
      this.flipData.actor.cardDeck.doFlipFromDeckOrEdge(flipData);
      this.onFlipClose();
    });

  }
  
  /* -------------------------------------------- */
  updateConsequenceMalus(event) {
    this.flipData.consequencesSelected = $('#consequenceSelectMalus').val();
    let malusConsequence = 0;
    for (let consequenceId of this.flipData.consequencesSelected) {
      let consequence = this.flipData.consequencesList.find( item => item._id == consequenceId);
      console.log(consequence, consequenceId);
      malusConsequence += SoSUtility.getConsequenceMalus( consequence.data.severity );      
    }
    $('#consequence-malus').text(malusConsequence);
    this.flipData.malusConsequence = malusConsequence;
    this.updateScoreBase();
  }

    /* -------------------------------------------- */
    updateConsequenceBonus(event) {
      this.flipData.consequencesSelected = $('#consequenceSelectBonus').val();
      let bonusConsequence = 0;
      for (let consequenceId of this.flipData.consequencesSelected) {
        let consequence = this.flipData.consequencesList.find( item => item._id == consequenceId);
        console.log(consequence, consequenceId);
        bonusConsequence += SoSUtility.getConsequenceBonus( consequence.data.severity );      
      }
      $('#consequence-bonus').text(bonusConsequence);
      this.flipData.bonusConsequence = bonusConsequence;
      this.updateScoreBase();
    }
  
  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);

    this.bringToTop();

    var dialog = this;
      
    function onLoad() {
      let flipData = dialog.flipData;
      dialog.updateFlip(flipData);
    }
  
    // Setup everything onload
    $(function () { onLoad(); });
    
    html.find('#statSelect').change((event) => {
      this.updateFlip(dialog.flipData );
    } );

    html.find('#consequenceSelectMalus').change((event) => {
      this.updateConsequenceMalus( event );
    } );
    html.find('#consequenceSelectBonus').change((event) => {
      this.updateConsequenceBonus( event );
    } );
    html.find('#wound-light-checkbox').change((event) => {
      this.updateScoreBase( event );
    } );      
    html.find('#wound-moderate-checkbox').change((event) => {
      this.updateScoreBase( event );
    } );      
    html.find('#wound-severe-checkbox').change((event) => {
      this.updateScoreBase( event );
    } );      
    html.find('#wound-critical-checkbox').change((event) => {
      this.updateScoreBase( event );
    } );      

    html.find('.class-view-deck').click((event) => {
      let flipData = this.flipData;
      flipData.modifier = html.find('#modifier').val();
      if ( flipData.mode == 'skill' || flipData.mode == 'weapon') {
        let statKey = $('#statSelect').val();
        flipData.stat = duplicate( flipData.statList[ statKey ] );
      }
      flipData.cardOrigin = "Deck";
      flipData.tn = (flipData.target) ? flipData.target.actor.data.data.scores.defense.value : $('#tn').val();
      dialog.flipData.actor.cardDeck.doFlipFromDeckOrEdge(flipData);
      dialog.onFlipClose();
    });
    

    
  }
}