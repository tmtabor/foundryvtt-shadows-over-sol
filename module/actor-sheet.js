/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { SoSUtility } from "./sos-utility.js";

/* -------------------------------------------- */
export class SoSActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["sos", "sheet", "actor"],
      template: "systems/foundryvtt-shadows-over-sol/templates/actor-sheet.html",
      width: 640,
      height: 720,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "stats" }],
      dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
      editStatSkill: false
    });
  }

  /* -------------------------------------------- */
  getData() {
    let data = super.getData();
    
    data.data.edgecard = this.actor.getEdgesCard();
    data.data.deckSize = this.actor.getDeckSize();

    data.data.skills   = this.actor.data.items.filter( item => item.type == 'skill').sort( (a, b) =>  { 
          if ( a.name > b.name ) return 1;
          return -1;
        });
    data.data.skill1 = data.data.skills.slice(0, Math.ceil(data.data.skills.length/2) )
    data.data.skill2 = data.data.skills.slice(Math.ceil(data.data.skills.length/2), data.data.skills.length )
    data.data.consequences = this.actor.data.items.filter( item => item.type == 'consequence').sort( (a, b) =>  { 
      if ( a.name > b.name ) return 1;
      return -1;
    });
    data.data.gears      = this.actor.data.items.filter( item => item.type == 'gear');
    data.data.weapons    = this.actor.data.items.filter( item => item.type == 'weapon');
    data.data.armors     = this.actor.data.items.filter( item => item.type == 'armor');
    data.data.totalEncumbrance = SoSUtility.computeEncumbrance(this.actor.data.items);
    data.data.wounds     = duplicate(this.actor.data.data.wounds);
    data.data.isGM       = game.user.isGM;
    data.data.currentWounds = this.actor.computeCurrentWounds();
    data.data.totalWounds = this.actor.data.data.scores.wound.value;

    data.data.subculture = this.actor.data.items.find( item => item.type == 'subculture');
    data.data.geneline   = this.actor.data.items.find( item => item.type == 'geneline');
    data.data.editStatSkill =  this.options.editStatSkill;
    console.log("stats", data);
    //data.stats  = duplicate(this.actor.stats);
    //data.scores = duplicate(this.actor.scores);

    return data;
  }

  /* -------------------------------------------- */
  async _onDrop(event) {
    super._onDrop(event);
  }

  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    //HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("item-id"));
      item.sheet.render(true);
    });
    html.find('.item-equip').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.equipObject( li.data("item-id") );
      this.render(true);
    });
    html.find('.item-worn').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.wornObject( li.data("item-id") );
      this.render(true);
    });
    
    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      SoSUtility.confirmDelete(this, li);
    });

    html.find('.stat-label a').click((event) => {
      let statName = event.currentTarget.attributes.name.value;
      this.actor.rollStat(statName);
    });
    html.find('.skill-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      const skill = this.actor.getOwnedItem(li.data("item-id"));
      this.actor.rollSkill(skill);
    });
    html.find('.weapon-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      const weapon = this.actor.getOwnedItem(li.data("item-id"));
      this.actor.rollWeapon(weapon);
    });
    html.find('.skill-value').change((event) => {
      let skillName = event.currentTarget.attributes.skillname.value;
      //console.log("Competence changed :", skillName);
      this.actor.updateSkill(skillName, parseInt(event.target.value));
    });
    html.find('.skill-xp').change((event) => {
      let skillName = event.currentTarget.attributes.skillname.value;
      //console.log("Competence changed :", skillName);
      this.actor.updateSkillExperience(skillName, parseInt(event.target.value));
    });
    html.find('.wound-value').change((event) => {
      let woundName = event.currentTarget.attributes.woundname.value;
      //console.log("Competence changed :", skillName);
      this.actor.updateWound(woundName, parseInt(event.target.value));
    });    
    html.find('.reset-deck-full').click((event) => {
      this.actor.resetDeckFull();
      this.render(true);
    });
    html.find('.draw-new-edge').click((event) => {
      this.actor.drawNewEdge();
      this.render(true);
    });
    html.find('.reset-deck').click((event) => {
      this.actor.resetDeck();
      this.render(true);
    });
    html.find('.discard-card').click((event) => {
      const cardName = $(event.currentTarget).data("discard");
      this.actor.discardEdge( cardName );
    });
    html.find('.consequence-severity').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("item-id"));
      let severity = $(event.currentTarget).val();
      this.actor.updateOwnedItem( { _id: item._id, 'data.severity': severity});
      this.render(true);
    });
    html.find('.lock-unlock-sheet').click((event) => {
      this.options.editStatSkill = !this.options.editStatSkill;
      this.render(true);
    });    
    html.find('.item-link a').click((event) => {
      const itemId = $(event.currentTarget).data("item-id");
      const item = this.actor.getOwnedItem(itemId);
      item.sheet.render(true);
    });    
    
  }

  /* -------------------------------------------- */
  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
