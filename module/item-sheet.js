import { SoSUtility } from "./sos-utility.js";

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class SoSItemSheet extends ItemSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
			classes: ["foundryvtt-shadows-over-sol", "sheet", "item"],
			template: "systems/foundryvtt-shadows-over-sol/templates/item-sheet.html",
			width: 550,
			height: 550
      //tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}]
		});
  }

  /* -------------------------------------------- */
  _getHeaderButtons() {
    let buttons = super._getHeaderButtons();
    // Add "Post to chat" button
    // We previously restricted this to GM and editable items only. If you ever find this comment because it broke something: eh, sorry!
    buttons.unshift(
      {
        class: "post",
        icon: "fas fa-comment",
        onclick: ev => {} //new RdDItem(this.item.data).postItem()
      })
    return buttons
  }

  /* -------------------------------------------- */
  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }
  
  /* -------------------------------------------- */
  async getData() {
    let data = super.getData();
    data.isGM = game.user.isGM; 

    return data;
  }
  
  /* -------------------------------------------- */
  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;
    
    
  }
  
  /* -------------------------------------------- */
  get template()
  {
    let type = this.item.type;
    return `systems/foundryvtt-shadows-over-sol/templates/item-${type}-sheet.html`;
  }

  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    return this.object.update(formData);
  }
}
