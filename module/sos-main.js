/**
 * RdD system
 * Author: LeRatierBretonnien
 * Software License: GNU GPLv3
 */

/* -------------------------------------------- */

/* -------------------------------------------- */
// Import Modules
import { SoSActor } from "./actor.js";
import { SoSItemSheet } from "./item-sheet.js";
import { SoSActorSheet } from "./actor-sheet.js";
import { SoSUtility } from "./sos-utility.js";
import { SoSCombat } from "./sos-combat.js";
import { gearConverter } from "./gears_convert.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

/************************************************************************************/
Hooks.once("init", async function () {
  console.log(`Initializing Shadows Over Sol System`);

  // preload handlebars templates
  SoSUtility.preloadHandlebarsTemplates();
  // Create useful storage space
  game.system.sos = {  }

  /* -------------------------------------------- */
  // Set an initiative formula for the system 
  CONFIG.Combat.initiative = {
    formula: "1d3",
    decimals: 2
  };

  /* -------------------------------------------- */
  game.socket.on("system.foundryvtt-shadows-over-sol", data => {
    SoSUtility.onSocketMesssage(data);
  });

  /* -------------------------------------------- */
  // Define custom Entity classes
  CONFIG.Actor.entityClass = SoSActor;
  CONFIG.Combat.entityClass = SoSCombat;
  CONFIG.SoS = {
  }

  /* -------------------------------------------- */
  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("foundryvtt-shadows-over-sol", SoSActorSheet, { types: ["character"], makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("foundryvtt-shadows-over-sol", SoSItemSheet, { makeDefault: true });

  // Init/registers
  Hooks.on('renderChatLog', (log, html, data) => {
    SoSUtility.registerChatCallbacks(html);
  });
  // Init/registers
  Hooks.on('updateCombat', (combat, round, diff, id) => {
    SoSUtility.updateCombat(combat, round, diff, id);
  });
  
});

/* -------------------------------------------- */
function welcomeMessage() {
  //ChatUtility.removeMyChatMessageContaining('<div id="welcome-message-sos">');
  ChatMessage.create({
    user: game.user._id,
    whisper: [game.user._id],
    content: `<div id="welcome-message-sos"><span class="rdd-roll-part">Welcome !</div>
    ` });
}

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.once("ready", function () {

  // User warning
  if (!game.user.isGM && game.user.character == undefined) {
    ui.notifications.info("Warning ! You are not linked to any actor !");
    ChatMessage.create({
      content: "<b>WARNING</b> Player " + game.user.name + " is not linked to an actor !",
      user: game.user._id
    });
  }
  
  //gearConverter.importgear();

  welcomeMessage();

});

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.on("chatMessage", (html, content, msg) => {
  if (content[0] == '/') {
    let regExp = /(\S+)/g;
    let commands = content.toLowerCase().match(regExp);
    if (game.system.sos.commands.processChatCommand(commands, content, msg)) {
      return false;
    }
  }
  return true;
});

/* -------------------------------------------- */
Hooks.on("getCombatTrackerEntryContext", (html, options) => {
  //SoS.pushInitiativeOptions(html, options);
}
)
